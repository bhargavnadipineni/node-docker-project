FROM node:21-alpine

# Copy only the necessary files for building the image
COPY package.json /app/
COPY models /app/models/
COPY routes /app/routes/
COPY app.js /app/

WORKDIR /app

RUN npm install

# Expose the port your app will run on
EXPOSE 3000

# Command to run the application
CMD ["node", "app.js"]
