const express = require('express')
const mongoose = require('mongoose')

const url = "mongodb://mongo:27017/TodoListNode"

const app = express()

mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true})

const con = mongoose.connection

con.on('open', ()=>{
    console.log("DB Connected Successfully");
})

app.use(express.json())

app.use('/api/v1/task', require('./routes/todolist'))
app.use('/api/v1/tasks', require('./routes/bulkupload'))

app.listen(3000, ()=>{
    console.log("Server Started at port 3000");
})

