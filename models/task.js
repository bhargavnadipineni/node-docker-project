const mongoose = require('mongoose')

const taskSchema = new mongoose.Schema({

    description:{
        type: String,
        required: [true, "Description is Required"]
    },

    status:{
        type: String,
        enum: ["pending", "completed"],
        default: "pending",
    },
    date: {
        type: Date,
        default: Date.now,
    }

})

module.exports = mongoose.model('Task', taskSchema)