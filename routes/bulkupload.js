const express = require('express')
const router = express.Router()
const Task = require('../models/task')


router.get('/', async (req, res)=>{
    try {
        const taskIdsToRetrieve = req.body.taskIds;
        const retrievedTasks = await Task.find({ _id: { $in: taskIdsToRetrieve } });
        res.status(200).json(retrievedTasks)
    } catch (error) {
        res.status(500).json(error)
    }
})

router.post('/', async(req,res)=>{

    try {
        const savedTasks = await Task.insertMany(req.body.tasks);
        res.status(201).json({success:true ,List:savedTasks})
    } catch (error) {
        res.status(500).json(error.errors.description)
    }
})





module.exports = router