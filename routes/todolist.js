const express = require('express')
const router = express.Router()
const Task = require('../models/task')



router.get('/', async (req, res)=>{
    try {
        const tasks = await Task.find()
        res.status(200).json(tasks)
    } catch (error) {
        res.status(500).json(error)
    }
})

router.post('/', async(req,res)=>{

    if(!req.body.description || req.body.description.length <5){
        return res.status(400).json({error: "Description of min 5 Characters required"})
    }

    try {
        const task = new Task({
            description: req.body.description
        })
        const savedTask = await task.save();
        res.status(201).json({success:true ,List:savedTask})
    } catch (error) {
        res.status(500).json(error)
    }
})

router.put('/:id', async(req,res)=>{
    try {
        
        const taskId = req.params.id
        let task = await Task.findById(taskId)
        if(!task){
            return res.status(404).json({message: `Task with ID: ${taskId} is not found`})
        }

        task.status = (task.status == "pending") ? "completed" : "pending"
        task.save()
        res.status(200).json({success:true, message:`Task with ID: ${taskId} Updated Successfully`, task: task})

    } catch (error) {
        res.status(500).json(error)
    }
})

router.delete('/:id', async(req,res)=>{
    try {
        const taskId = req.params.id
        let task = await Task.findById(taskId)
        if(!task){
            return res.status(404).json({message: `Task with ID: ${taskId} is not found`})
        }
        
        task = await Task.findByIdAndDelete(taskId)

        res.status(200).json({success:true, message:`Task with ID: ${taskId} Deleted Successfully`, task: task})

    } catch (error) {
        res.status(500).json(error)
    }
})

module.exports = router